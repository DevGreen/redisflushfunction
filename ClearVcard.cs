using System;
using System.Linq;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;

namespace RedisFlushFunction
{
    public static class ClearVcard
    {
        [FunctionName("ClearVcard")]
        public static void Run([TimerTrigger("0 0 * * * *")] TimerInfo myTimer, ILogger log)
        {
            var redisConnString = System.Environment.GetEnvironmentVariable("redisConnString");
            var redisServer = System.Environment.GetEnvironmentVariable("redisServer");

            log.LogInformation($"Clear VCard Timer trigger function executed at: {DateTime.Now}");
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(redisConnString);
            var server = redis.GetServer(redisServer);

            long current_cursor = 0;

            int next_cursor = -1;
            int page_size = 1000;
            int i = 0;

            var deletablePrefix = MakeDeletablePrefix(DateTime.UtcNow);

            var pattern = $"VCard:{deletablePrefix}*";

            while (next_cursor != 0)
            {
                var allkeys = server.Keys(0, pattern, page_size, 0, next_cursor == -1 ? 0 : next_cursor);

                var cursor = ((StackExchange.Redis.IScanningCursor)allkeys);
                if (!allkeys.Any())
                {
                    break;
                }
                foreach (var key in allkeys)
                {
                    if (current_cursor == cursor.Cursor)

                    {
                        //log.LogInformation($"{key}");
                        redis.GetDatabase().KeyDelete(key);
                        i++;

                    }
                    else
                    {
                        next_cursor = Convert.ToInt32(cursor.Cursor);
                        current_cursor = next_cursor;

                        break;

                    }
                }
            }



            log.LogInformation($"VCard Cleared: {i}");

            var memoryStats = server.MemoryStats().ToDictionary();
            int total = (int)memoryStats["total.allocated"];

            log.LogInformation($"Current Memory Usage: {total} bytes");

        }



        //Should return a prefix with the following guarantees
        //1: No keys generated in the last 10 min have the same prefix
        //2: Will match keys in a 114 minute window occurring before now
        //3:  228 min ago <  Window Start < 124 min ago
        //4:  114 min ago < Window End  < 10 min ago
        //5: If you delete matching keys once per hour you should get them all
        private static string MakeDeletablePrefix(DateTime utcNow)
        {

            var nowPrefix = GetKeyPrefix(utcNow);
            var tenMinAgoPrefix = GetKeyPrefix(utcNow.AddMinutes(-10));

            if (nowPrefix == tenMinAgoPrefix) //Prefix hasn't changed in last 10 minutes
            {
                //Use previous Prefix
                //115 min ago is guaranteed to have been during the previous prefix
                return GetKeyPrefix(utcNow.AddMinutes(-115));
            }
            else
            {
                //Prefix recently changed.  Get the one before the last one

                return GetKeyPrefix(utcNow.AddMinutes(-125));

            }
        }

        //Should be different every 114 min
        private static string GetKeyPrefix(DateTime utcTime)
        {
            var fullKey = GenerateDescendingKey(utcTime);
            var prefix = fullKey.Substring(0, 5);
            return prefix;
        }


        private const long _upperBound = 700000000000000000;// 17 Zeros

        //Fifth digit changes every 114 min
        //Sixth digit changes every 7.125 min
        private static string GenerateDescendingKey(DateTime utcTime)
        {
            long ticks = utcTime.Ticks;
            long number = _upperBound - ticks;
            string key = number.ToString("X14");
            return key;
        }
    }
}
