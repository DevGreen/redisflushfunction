using System;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using System.Net;
using StackExchange.Redis;


namespace RedisFlushFunction
{
    public static class FlushRedisDatabase
    {
        [FunctionName("FlushRedisDatabase")]
        public static async Task Run([TimerTrigger("0 15 */6 * * *")] TimerInfo myTimer, ILogger log)
        {
            var redisConnString = System.Environment.GetEnvironmentVariable("redisConnString");
            var redisServer = System.Environment.GetEnvironmentVariable("redisServer");

            log.LogInformation($"FlushRedisDatabase Timer trigger function executed at: {DateTime.Now}");
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(redisConnString);
            var server = redis.GetServer(redisServer);
            var memoryStats = server.MemoryStats().ToDictionary();
            int total = (int)memoryStats["total.allocated"];

            log.LogInformation($"Current Memory Usage: {total} bytes");
            if (total > 400000000)
            {
                log.LogInformation($"Memory is over 400megs, Flushing Database");
                server.FlushDatabase(0);
            }
        }
    }
}